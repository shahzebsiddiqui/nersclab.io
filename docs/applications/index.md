# Applications

NERSC provides a wide variety of pre-built applications, optimized for
our systems. The primary way these are provided in the [NERSC
environment](../environment/index.md) is through
[modules](../environment/#nersc-modules-environment).

!!! tip "Best practices"
    [Best practices](../jobs/best-practices.md) are not specific to any
    one application and ensure efficient use your project's allocation.

!!! note
    Some available applications may be present, but may not
    necessarily have a dedicated page on this site. Please consult the
    `module avail` command on a NERSC system to see what is available.

In addition to application specific details NERSC also provides:

* additional [example jobs](../jobs/examples/index.md)
* [workflow tools](../jobs/workflow-tools.md) for coordination/ automation.
* A [guide](../getting-started.md) for new users.

## Popular applications

### Density functional theory

* [VASP](vasp/index.md)
* [Quantum ESPRESSO](quantum-espresso/index.md)
* [SIESTA](siesta/index.md)
* [WIEN2k](wien2k/index.md)
* [CP2K](cp2k/index.md)

### Molecular dynamics

* [AMBER](amber/index.md)
* [NAMD](namd/index.md)
* [Gromacs](gromacs/index.md)
* [LAMMPS](lammps/index.md)

### Chemistry applications

* [GAMESS](gamess/index.md)
* [MOLPRO](molpro/index.md)
* [Q-Chem](qchem/index.md)

### Visualization

* [VisIt](visit/index.md)
